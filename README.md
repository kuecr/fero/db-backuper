# dbbackuper: herramienta a medida para guardar copias de seguridad de bases de datos Postgres

## variables de entorno

- `OS_USERNAME`: nombre de usuario en Rackspace Cloud Files (requerido por `rclone`)
- `OS_PASSWORD`: contraseña del usuario en Rackspace Cloud Files (requerido por `rclone`)
- `OS_REGION`: región donde existe la nube (requerido por `clone`, posibles valores: Dallas (`DFW`), Chicago (`ORD`), North Virginia (`IAD`), Hong Kong (`HKG`), o Sydney, Australia (`SYD`), ver [Quickstart for Cloud Files](https://docs.rackspace.com/docs/cloud-files/quickstart/#authentication))
- `BACKUP_LOCATION`: ubicación remota en RSC donde se guardan las copias de seguridad (requerido)
- `BACKUP_DIR`: ubicación local donde se vuelcan las bases de datos (requerido, aqui se sincroniza los archivos remotos también)
- `PGHOST` ubicacion remota de la base de datos (requerido por `psql` y `pg_dump`)
- `PGPORT` puerto de la base de datos remota, 5432 por default (usado por `psql` y `pg_dump`)
- `PGUSER` usuario para la conexión (requerido por `psql` y `pg_dump`)
- `PGPASSWORD` contraseña del usuario para la conexión (requerido por `psql` y `pg_dump`)

Para más información ver [Environment Variables, postgres documentation](https://www.postgresql.org/docs/current/libpq-envars.html).
