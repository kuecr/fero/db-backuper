#!/usr/bin/env bash
set -euo pipefail

# crea configuracion de rclone
./setup_cfg.sh
# sincroniza las copias remotas en el volumen
./setup_sync.sh
# vuelca todas las bases de datos y las sube a Rackspace
./dump.sh
