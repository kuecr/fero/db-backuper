#!/usr/bin/env bash
set -euo pipefail

mkdir -p $BACKUP_DIR;
echo -e "[rackspace]\ntype = swift\nuser = $OS_USERNAME\nkey = $OS_PASSWORD\nauth = https://identity.api.rackspacecloud.com/v2.0\nregion = $OS_REGION\n" > /.rclone.conf;
