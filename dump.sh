#!/usr/bin/env bash
set -euo pipefail

# recupera lista de bases de datos
DBList=$(psql -t -f ./find_dbs.sql)

for db in $DBList; do
    echo "haciendo copia de seguridad para base de datos $db";
    # TODO: Comprimir el contenido
    pg_dump -Fp -C $db > $BACKUP_DIR/$db-$(date "+%m-%d-%Y_%H_hours").sql;
done

rclone -v sync $BACKUP_DIR/ rackspace:$BACKUP_LOCATION;
