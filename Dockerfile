FROM bitnami/rclone:latest

ARG GIT_COMMIT=unspecified
LABEL git_commit=$GIT_COMMIT

USER root
RUN install_packages gnupg2 wget && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" | tee  /etc/apt/sources.list.d/pgdg.list && \
    apt update && \
    install_packages postgresql-client-13

COPY backup.sh ./
COPY setup_cfg.sh ./
COPY setup_sync.sh ./
COPY dump.sh ./
COPY find_dbs.sql ./

ENTRYPOINT ["/backup.sh"]
