#!/usr/bin/env bash
set -euo pipefail

# sincroniza el contenido remoto localmente
rclone -v sync rackspace:$BACKUP_LOCATION $BACKUP_DIR;

# borra cualquier cosa con mas de 30 dias de edad
find $BACKUP_DIR -mtime +30 -delete;
