##
# dbbackuper
#
# @file
# @version 0.1

build:
	docker -t shackra/dbbackup:latest --build-arg GIT_COMMIT=$(git log -1 --format=%h) .

push: build
	docker push -a shackra/dbbackup

# end
